namespace feng3d
{
    export class ShaderToyShader extends Shader
    {
        setShaderToyCode(code: string)
        {
            var s = getShaderCode(code);
            this.setShader(s.vertex, s.fragment);
            return this;
        }
    }
}

function getShaderCode(code: string)
{
    return {
        vertex: `#version 300 es
        #ifdef GL_ES
            precision highp float;
            precision highp int;
            precision mediump sampler3D;
        #endif
        layout(location = 0) in vec2 pos;
        void main() {
            gl_Position = vec4(pos.xy, 0.0, 1.0);
        }
        
    `,
        fragment: `#version 300 es
        #ifdef GL_ES
            precision highp float;
            precision highp int;
            precision mediump sampler3D;
        #endif
        #define HW_PERFORMANCE 1
        uniform vec3      iResolution;
        uniform float     iTime;
        uniform float     iChannelTime[4];
        uniform vec4      iMouse;
        uniform vec4      iDate;
        uniform float     iSampleRate;
        uniform vec3      iChannelResolution[4];
        uniform int       iFrame;
        uniform float     iTimeDelta;
        uniform float     iFrameRate;
        uniform sampler2D iChannel0;
        uniform struct {
            sampler2D sampler;
            vec3  size;
            float time;
            int   loaded;
        }
        iCh0;
        uniform sampler2D iChannel1;
        uniform struct {
            sampler2D sampler;
            vec3  size;
            float time;
            int   loaded;
        }
        iCh1;
        uniform sampler2D iChannel2;
        uniform struct {
            sampler2D sampler;
            vec3  size;
            float time;
            int   loaded;
        }
        iCh2;
        uniform sampler2D iChannel3;
        uniform struct {
            sampler2D sampler;
            vec3  size;
            float time;
            int   loaded;
        }
        iCh3;
        void mainImage( out vec4 c, in vec2 f );

        ${code}

        out vec4 outColor;
        void main( void ) {
            vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
            mainImage( color, gl_FragCoord.xy );
            color.w = 1.0;
            outColor = color;
        }
        
    `};
}